package Com.utility;

import org.openqa.selenium.Alert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Com.pageFactory.testbase;

public class ClsAlert extends testbase{
	
	
	public static void alertAccept()
	{
		 try {
		       wait = new WebDriverWait(driver, 2);
		        wait.until(ExpectedConditions.alertIsPresent());
		        Alert alert = driver.switchTo().alert();
		        alert.accept();
		    } 
		    catch (Exception e)
		    {
		        System.out.println(e.toString());
		    }
		
		
	}
	public static void alertDennied()
	{
		 try {
		       wait = new WebDriverWait(driver, 2);
		        wait.until(ExpectedConditions.alertIsPresent());
		        Alert alert = driver.switchTo().alert();
		        alert.dismiss();
		    } 
		    catch (Exception e)
		    {
		    	 System.out.println(e.toString());
		    }
		
		
	}

}

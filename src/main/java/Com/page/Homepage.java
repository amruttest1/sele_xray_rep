package Com.page;

import static org.testng.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import Com.pageFactory.testbase;

public class Homepage extends testbase{
	WebDriver driver;
	
	@FindBy(xpath ="/html/body/div[1]/table/tbody/tr/td[1]")
	WebElement ele_Todo;
	
	public Homepage(WebDriver driver){
		this.driver = driver;
}
		
	public void verifyHomepage() throws FileNotFoundException, IOException {
	
		WebElement ele_Todo= driver.findElement(By.xpath("/html/body/div[1]/table/tbody/tr/td[1]"));
		ele_Todo.getText();
		assertEquals(ele_Todo.getText(), getdata("asserText"));
		System.out.println("Element  present ...."+ele_Todo.getText());
	}


}

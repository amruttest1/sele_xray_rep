package com.Devopslabs;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class TC_LoginLogout {
	public static WebDriver driver= null;
	SoftAssert assertion= new SoftAssert();
	
	
	@FindBy(xpath="/html/body/div[1]/table/tbody/tr/td[1]")
    WebElement ele_Todo;
	
	
//	Defining driver and url
	//@BeforeClass
	 public static WebDriver chromeBrowser() 
	 { 	
			if(driver == null){
			
				System.setProperty("webdriver.gecko.driver", "geckodriver.exe");			
				driver= new FirefoxDriver();					
				String baseUrl= "http://devopslabs.co:8888/welcome";
				driver.get(baseUrl);
			}
			return driver;
	 	}
	//login to jira 
	//	@Test(priority = 1)
		 public void TC_001_VarifyToDo()
		 {	
			driver=chromeBrowser() ;
		 	 try
		 	 {  
		 		 WebElement ele_Todo=driver.findElement(By.xpath("/html/body/div[1]/table/tbody/tr/td[1]"));
		 		 System.out.println(ele_Todo.getText());
				assertion.assertTrue(ele_Todo.isDisplayed(),ele_Todo.getText());
				assertion.assertAll();
				//logout();
	 	 }
	 	catch(Exception e)
	 	{
	 		e.printStackTrace();
	 		
	 	}
	}
//	
	//@AfterClass
	public void BrowserClose()
	{
		driver.close();
	}
	
}
